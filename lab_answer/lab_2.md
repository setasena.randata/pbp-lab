1. Apakah perbedaan antara JSON dan XML?

Menelusuri lebih dalam mengenai perbedaan XML dan JSON, alangkah lebih baik bagi saya untuk memahami terlebih dahulu singkatan dari keduanya. XML adalah eXtensible Markup Language, sementara JSON adalah JavaScript Object Notation. Mungkin secara visual, kita bisa menganggap bahwa tingkat kompleksitas XML lebih simpel. Namun, 
sebenarnya dari segi implementasi, akan jauh lebih familiar JSON karena dinamikanya menggunakan object.
Pengimplementasian XML yang berbentuk seperti HTML tags terkadang memberikan kesan asing dan untuk memahaminya
tidak secepat JSON.

2. Apakah perbedaan antara HTML dan XML?

Perbedaan HTML dan XML dapat kita pahami dengan mengetahui terlebih dahulu pengertian dari HTML. HTML adalah
Hypertext Markup Language. Melalui definisi, kita dapat mengetahui bahwa HTML adalah sebuah markup language,
sementara XML adalah markup language yang mendefinisikan markup language lainnya (extensible). Lebih detail,
penulisan HTML juga tidak case sensitive, sementara XML case sensitive.


Referensi:
https://hackr.io/blog/json-vs-xml
https://www.upgrad.com/blog/html-vs-xml/