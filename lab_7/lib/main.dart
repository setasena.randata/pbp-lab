import 'package:flutter/material.dart';

TextEditingController controllerNama = TextEditingController();
String display = '';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BelajarFlutter.com"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              TextFormField(
                controller : controllerNama,
                decoration: new InputDecoration(
                hintText: "contoh: Buku ini start with why bagus banget beneran!",
                labelText: "Komentar kamu tentang buku start with why",
                icon: Icon(Icons.add_comment),
                border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (String? value) {
                  if (value != null && value.isEmpty) {
                    return 'Komentar kamu tidak boleh kosong';
                  }
                  return null;
                },
              ),
              SizedBox(height: 36),
              RaisedButton(
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                      return setState((){
                          display = controllerNama.text;
                    });
                  }
                },
              ),
              SizedBox(height: 108),
              if (display == '') ...[
                Text(
                    'Hello, bagaimana tanggapan kamu tentang buku start with why?',
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
              ] else ...[
                Text(
                    'Komentar aku tentang buku start with why?: $display',
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
              ] 
            ],
          ),
        ),
      ),
    );
  }
}

// referensi pembuatan conditional formatting / if statement: https://fluttercorner.com/how-to-use-conditional-statement-in-widget-in-flutter/
// referensi pembuatan form: https://belajarflutter.com/tutorial-cara-membuat-form-di-flutter-lengkap/  
// referensi menyimpan valeu dari controller: https://stackoverflow.com/questions/61538657/how-to-get-value-from-textformfield-on-flutter

