from django.http import response
from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
# Create your views here.

@login_required(login_url='admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)
    if (request.method == 'POST') & (form.is_valid()):
        form.save()
        return index(request)
    
    response = {'form' : form}

    return render(request, 'lab3_form.html', response)
