import 'package:flutter/material.dart';

import '../dummy_data.dart';
import '../widgets/main_drawer.dart';
import '../widgets/picks_item.dart';

class PicksScreen extends StatefulWidget {
  static const routeName = '/picks';

  @override
  _PicksScreenState createState() => _PicksScreenState();
}

class _PicksScreenState extends State<PicksScreen> {
  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: const EdgeInsets.all(25),
      children: DUMMY_PICKS
          .map(
            (catData) => PicksItem(
                  catData.id,
                  catData.title,
                  catData.color,
                ),
          )
          .toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
    );
  }
}
