import 'package:flutter/material.dart';

class Picks {
  final String id;
  final String title;
  final Color color;

  const Picks({
    @required this.id,
    @required this.title,
    this.color = Colors.orange,
  });
}
